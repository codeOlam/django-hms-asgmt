from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import PatientProfile


class PtnUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = PatientProfile
        fields = ('ptn_user',
                  'slug_ptn_profile',
                  'first_name',
                  'last_name',
                  'other_name',
                  'dateOfBirth',
                  'stateOfResidence',
                  'age',
                  'sex',
                  'diagnosed',
                  'blood_group',
                  'allergies',
                  'phone_number',
                  'photo',
                  )



class PtnUserChangeForm(UserChangeForm):

    class Meta:
        fields = ('ptn_user',
                  'slug_ptn_profile',
                  'first_name',
                  'last_name',
                  'other_name',
                  'dateOfBirth',
                  'stateOfResidence',
                  'age',
                  'sex',
                  'diagnosed',
                  'blood_group',
                  'allergies',
                  'phone_number',
                  'photo',
                  )