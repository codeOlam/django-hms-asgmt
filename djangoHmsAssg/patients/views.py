from django.shortcuts import render
from django.views.generic import DetailView, TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse

from .models import PatientProfile

# Create your views here.
class CreatePatientProfile(CreateView):
	pass

class PatientProfileList(ListView):
	model 				= PatientProfile
	context_object_name = 'patient_list'
	template_name		= 'patients/list_ptn.html'

class PatientProfileDetail(DetailView):
	model 				= PatientProfile
	context_object_name = 'patient'
	slug_field			= 'slug_ptn_profile'
	slug_url_kwarg		= 'slug_ptn_profile'
	template_name 		= 'patients/ptn_profile.html'


class UpdatePatientProfile(UpdateView):
	model 				= PatientProfile
	fields 				= '__all__'
	slug_field			= 'slug_ptn_profile'
	slug_url_kwarg		= 'slug_ptn_profile'
	template_name 		= 'patients/ptn_profile.html'