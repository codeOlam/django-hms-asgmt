from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, username, password=None):
        """
        Creates and saves a User with the given username and password.
        """
        if not username:
            raise ValueError('Please provide a username to continue')

        user = self.model(
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
            username,
        )
        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUserModel(AbstractBaseUser):
    sys_id 		    = models.AutoField(primary_key=True, blank=True)
    username        = models.CharField(max_length=50, blank=False, unique=True, null=False)
    is_admin 		= models.BooleanField(default=False)
    is_active 		= models.BooleanField(default=True)


    objects = CustomUserManager()

    USERNAME_FIELD = "username"


    class Meta:
        app_label = "accounts"
        db_table = "users"

    def __str__(self):
        return self.username

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def has_perm(self, perm, obj=None):
        #User has a specific permission
        return True

    def has_module_perms(self, app_label):
        #User have permission to view app label
        return True

    @property
    def is_staff(self):
        #Check if user is staff member
        return self.is_admin
