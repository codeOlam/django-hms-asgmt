from django.views.generic.edit import CreateView
# from django.urls import reverse_lazy, reverse

from .forms import UserCreationForm
# Create your views here

class CreateProfile(CreateView):
    form_class          = UserCreationForm
    template_name       = 'accounts/registration/signup.html'
    success_url         = '/login'
