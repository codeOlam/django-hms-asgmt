from django.contrib import admin


from .forms import PtnUserChangeForm, PtnUserCreationForm
from .models import PatientProfile

# Register your models here.
class PatientProfileAdmin(admin.ModelAdmin):
	add_form 		= PtnUserCreationForm
	form 			= PtnUserChangeForm
	model 			= PatientProfile
	list_display 	= ('first_name', 'last_name', 'sex', 'diagnosed', 'stateOfResidence')

admin.site.register(PatientProfile, PatientProfileAdmin)