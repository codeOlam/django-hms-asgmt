from django.urls import path


from .views import PatientProfileDetail, PatientProfileList, UpdatePatientProfile

urlpatterns = [
	path('profile/', PatientProfileList.as_view(), name='list_ptn'),
	path('profile/<slug_ptn_profile>/update_profile/', UpdatePatientProfile.as_view(), name='ptn_profile'),
 	path('profile/<slug_ptn_profile>/', PatientProfileDetail.as_view(), name='ptn_profile'),
 ]