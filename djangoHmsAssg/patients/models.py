from django.conf import settings
from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify
import datetime
from django.utils import timezone
import random
import string


# Create your models here.
customUser = settings.AUTH_USER_MODEL

diagnoses = (('Cancer', 'Cancer'),
			('Laser Fever', 'Laser Fever'),
			('COVID-19', 'Covid-19'),
			('Ebola', 'Ebola'),
			('SARS', 'SARS'))


def randomStrDig(strlen=4):
	"""
		Convenient methods to autogenerate string
	"""
	alpha_num 	= string.ascii_letters + string.digits

	return ''.join(random.choice(alpha_num) for i in range(strlen)).lower()

gen_str = randomStrDig()


class PatientProfile(models.Model):
	ptn_user 					= models.OneToOneField(customUser, on_delete=models.CASCADE)
	slug_ptn_profile			= models.SlugField(max_length=55, null=True, blank=True, help_text="Leave it blank will autogenerate")
	first_name 					= models.CharField(max_length=50, blank=False)
	last_name 					= models.CharField(max_length=50, blank=False)
	other_name 					= models.CharField(max_length=50, blank=True, null=True)
	dateOfBirth 				= models.DateField()
	stateOfResidence 			= models.CharField(max_length=100, blank=False)
	age 						= models.PositiveIntegerField(blank=False)
	sex 						= models.CharField(max_length=7, blank=False)
	diagnosed                	= models.CharField(max_length=15, choices=diagnoses, blank=False)
	blood_group 				= models.CharField(max_length=5, blank=False)
	allergies 					= models.TextField()
	phone_number				= models.CharField(max_length=15, blank=False)
	photo 						= models.ImageField(upload_to='profile_pic', height_field=None, width_field=None, max_length=100, null=True, blank=True)
	add_timestamp 				= models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return self.first_name

	def get_absolute_url(self):
		return reverse('patients:ptn_profile', kwargs={'slug_ptn_profile': self.slug_ptn_profile})


	class Meta:
		verbose_name 			= 'Patient'
		verbose_name_plural 	= 'Patients'


@receiver(pre_save, sender=PatientProfile)
def aut_gen_slug(sender, instance, **kwargs):
	instance.slug_ptn_profile = slugify(instance.first_name+'_'+gen_str)